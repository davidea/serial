VERSION = 1.0.0
REFIX  = /usr

//CC      = gcc
CC	= mipsel-openwrt-linux-musl-gcc
CFLAGS  = -W  -Wextra \
        -DVERSION=\"$(VERSION)\" \
        -g
LDFLAGS =

all: serial

serial: serial.c
	$(CC) -o $@ $(CFLAGS) $(LDFLAGS) $^

clean:
	rm -f serial

install: all
	scp serial root@192.168.16.1:/root/
