
static void usage(const char * const argv0)
{
	printf("Usage: %s -d <modem_device> -c <AT command> -v verbose -u human readable output -s signal\n", argv0);
	printf("\n");
	printf("\t<modem_device> is the device file for the modem (e.g. /dev/ttyACM0, /dev/ttyS0, etc).\n");
	printf("\t<AT command> is the command we must supply to the modem.\n");
	printf("\t-v option to see the debug output.\n");
	printf("\t-s option to see the rf signal .\n");
	printf("\t-u option to see the output in human readable format.\n");
	printf("\n");
	printf("In addition the program supports the following options:\n");
	printf("\n");
	printf("\t-h|--help\n");
	printf("\t-V|--version\n");
	printf("\t--usage\n");
	printf("\n");
}

static void help(const char * const argv0)
{
	printf("This program takes AT commands as input, sends them to the modem and outputs\n");
	printf("the responses.\n");
	printf("\n");
	printf("Example:\n");
	printf("\n");
	printf("# %s -d /dev/ttyUSB2 -c AT^HCSQ=? \n", argv0);
	printf("\n");
	printf("^HCSQ: \"NOSERVICE\",\"GSM\",\"WCDMA\",\"LTE\"\n");
	printf("\n\n\n");
	printf("# %s -d /dev/ttyUSB2 -c at^HCSQ? -s -u\n",argv0);
	printf("\n");
	printf("human readable output\n");
	printf(" lte rssi -81.000000\n");
	printf(" lte rsrp -108.000000\n");
	printf(" lte sinr 8.200000\n");
	printf(" lte rsrq -6.500000\n");

}


static bool add_cr(const char *s)
{
	char *p;
	p = strchr(s, 0x0);

	*p = '\r';
	return true;
}




int set_interface_attribs (int fd, int speed, int parity)
{
        struct termios tty;
        memset (&tty, 0, sizeof tty);
        if (tcgetattr (fd, &tty) != 0)
        {
                printf ("error %d from tcgetattr", errno);
                return -1;
        }

        cfsetospeed (&tty, speed);
        cfsetispeed (&tty, speed);

        tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;     // 8-bit chars
        // disable IGNBRK for mismatched speed tests; otherwise receive break
        // as \000 chars
        tty.c_iflag &= ~IGNBRK;         // disable break processing
        tty.c_lflag = 0;                // no signaling chars, no echo,
                                        // no canonical processing
        tty.c_oflag = 0;                // no remapping, no delays
        tty.c_cc[VMIN]  = 0;            // read doesn't block
        tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

        tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

        tty.c_cflag |= (CLOCAL | CREAD);// ignore modem controls,
                                        // enable reading
        tty.c_cflag &= ~(PARENB | PARODD);      // shut off parity
        tty.c_cflag |= parity;
        tty.c_cflag &= ~CSTOPB;
        tty.c_cflag &= ~CRTSCTS;

        if (tcsetattr (fd, TCSANOW, &tty) != 0)
        {
                printf ("error %d from tcsetattr", errno);
                return -1;
        }
        return 0;
}


void set_blocking (int fd, int should_block)
{
        struct termios tty;
        memset (&tty, 0, sizeof tty);
        if (tcgetattr (fd, &tty) != 0)
        {
                printf ("error %d from tggetattr", errno);
                return;
        }

        tty.c_cc[VMIN]  = should_block ? 1 : 0;
        tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

        if (tcsetattr (fd, TCSANOW, &tty) != 0)
                printf ("error %d setting term attributes", errno);
}



// test function to print the single characters and the hex value for the string passed
void print_hex(char *original_string) {

  unsigned int i=0;
  char string[1000]="";
	char car;
  strncpy(string,original_string,99);
	printf("\nthe string passed to the print_hex function is long %d byte",strlen(string));
  for (i=0;i<1+strlen(string);i++ ){
		if(string[i]==0x0d || string[i]==0x0a )
			{car=0x20;} else {
				car=string[i];
			}
		printf("\n i= %d char = %c hex = %x",i,car,string[i]);
  }
}



//extract the response line from the modem output delimited from cr
void response_line(char * response, char * line) {
int lenght = 0;
int ret=0,start=0,end=0,i=0,pos=0;
lenght = strlen(response);
for (i=0;i<=lenght;i++) {
  if (response[i]==0x0a) {ret++;}
  if (response[i]==0x0a && start!=0 ) {end=i;}
  if(ret==3&& start==0) {start=i;}
  if (start!=0 && end==0 && response[i]!=0x0a) {
    line[pos]=response[i];
		line[pos+1]=0x00;
    pos++;
    }
  }

}

//extract the transmssion mode from the response
//between "
void mode(char * response, char * line) {
int lenght = 0;
int ret=0,start=0,end=0,i=0,pos=0;
lenght = strlen(response);
for (i=0;i<=lenght;i++) {
  if (response[i]==0x22) {ret++;}
  if (response[i]==0x22 && start!=0 ) {end=i;}
  if(ret==1&& start==0) {start=i;}
  if (start!=0 && end==0 && response[i]!=0x22) {
    line[pos]=response[i];
		line[pos+1]=0x00;
    pos++;
    }
  }

}

//in the response ex:  ^HCSQ: "LTE",41,35,111,22
//extract the numeric value between two comma character , starting from the posiz comma

int valore(char * response, int posiz) {
int lenght = 0;
int ret=0,start=0,end=0,i=0,pos=0;
char line[4];
lenght = strlen(response);
for (i=0;i<=lenght;i++) {
  if (response[i]==44) {ret++;}
  if (response[i]==44 && start!=0 ) {end=i;}
  if(ret==posiz&& start==0) {start=i;}
  if (start!=0 && end==0 && response[i]!=44) {
    line[pos]=response[i];
		line[pos+1]=0x00;
    pos++;
    }
  }
return (int) strtol(line,NULL,10);
}

//convert the numeric value into the response to a radio value

//we must check the variabile name and the return string (at this moment temperature)

void signal(char * buffer,char * output,int human_readable,const char * hostname) {
char modo[10];
mode(buffer,modo);

/* table extracted from the huawei 909 manual
	#<sysmode>   <value1>     <value2>     <value3>   <value4>   <value5>

#"NOSERVICE"   -              -            -          -          -
#"GSM"       gsm_rssi         -            -          -          -
#"WCDMA"    wcdma_rssi  wcdma _rscp    wcdma_ecio     -          -
#"LTE"       lte_rssi     lte_rsrp      lte_sinr   lte_rsrq      -

# rssi       0 = -120         = -120 + x

# wcdma_rscp 0 = -120         = -120 + x
# lte_rsrp   0 = -140         = -140 + x

# wcdma_ecio 0 = -32          = -32 + (x/2)
# lte_sinr   0 = -20          = -20 + (x * 0.2)

lte_rsrq 		 0 = -19.5				= -19.5 + (x/2)
*/
//^HCSQ: "LTE",41,35,111,22
if (strcmp(modo, "LTE") == 0) {
		 float lte_rssi=0,lte_rsrp=0,lte_sinr=0,lte_rsrq=0;

		 lte_rssi=-120+valore(buffer,1);
		 lte_rsrp=-140+valore(buffer,2);
		 lte_sinr=-20+(valore(buffer,3)*0.2);
		 lte_rsrq=-19.5+(valore(buffer,4)/2);

		 if (human_readable==1) {
			 printf("\nhuman readable output");
			 printf("\ncollectd hostname : %s",hostname);
			 printf("\n lte rssi %f",lte_rssi);
			 printf("\n lte rsrp %f",lte_rsrp);
			 printf("\n lte sinr %f",lte_sinr);
			 printf("\n lte rsrq %f\n",lte_rsrq);
			 strncpy(output,"",1);
	 			} else {
		 		 sprintf(output,
					 "PUTVAL \"%s/exec-lte/lte_rssi\" N:%3.1f\nPUTVAL \"%s/exec-lte/lte_rsrp\" N:%3.1f\nPUTVAL \"%s/exec-lte/lte_sinr\" N:%3.1f\nPUTVAL \"%s/exec-lte/lte_rsrq\" N:%3.1f\nPUTVAL \"%s/exec-lte/lte_sum\" N:%3.1f:%3.1f:%3.1f:%3.1f\n",hostname,lte_rssi,hostname,lte_rsrp,hostname,lte_sinr,hostname,lte_rsrq,hostname,lte_rssi,lte_rsrp,lte_sinr,lte_rsrq);
					 //
					 //
		 			}
		}
/*
		# rssi       0 = -120         = -120 + x

		# wcdma_rscp 0 = -120         = -120 + x

		# wcdma_ecio 0 = -32          = -32 + (x/2)

*/

		if (strcmp(modo, "WCDMA") == 0) {
				 float wcdma_rssi=0,wcdma_rscp=0,wcdma_ecio=0;

				 wcdma_rssi=-120+valore(buffer,1);
				 wcdma_rscp=-120+valore(buffer,2);
				 wcdma_ecio=-20+(valore(buffer,3)/2);
				 if (human_readable==1) {
					 printf("\nhuman readable output");
					 printf("\n wcdma rssi %f\n",wcdma_rssi);
					 printf("\n wcdma rscp %f\n",wcdma_rscp);
					 printf("\nwcdma ecio %f\n",wcdma_ecio);
				 	 strncpy(output,"",1);
			 			} else {
						 //^HCSQ: "WCDMA",35,28,51
						 //PUTVAL "/exec-test/temperature" N:33.6
						 sprintf(output,"PUTVAL \"%s/exec-wcdma/wcdma_rssi\" N:%3.1f\nPUTVAL \"%s/exec-wcdma/wcdma_rscp\" N:%3.1f\nPUTVAL \"%s/exec-wcdma/wcdma_ecio\" N:%3.1f\n",hostname,wcdma_rssi,hostname,wcdma_rscp,hostname,wcdma_ecio);
						 }
				}

				if (strcmp(modo, "GSM") == 0) {
						 float gsm_rssi=0;

						 gsm_rssi=-120+valore(buffer,1);
						 if (human_readable==1) {
							 printf("\nhuman readable output");
							 printf("\n gsm rssi %f\n",gsm_rssi);
							 strncpy(output,"",1);
								} else {
								 //^HCSQ: "WCDMA",35,28,51
								 //PUTVAL "/exec-test/temperature" N:33.6
								 sprintf(output,"PUTVAL \"%s/exec-gsm/gsm_rssi\" N:%3.1f\n",hostname,gsm_rssi);
								 }
						}

}

int write_log(char * output) {

	FILE *f = fopen ("/tmp/serial.log", "a" );
		if (f == NULL)
		{
	        	printf ("error %d opening file: %s", errno,  strerror (errno));
		        return 0;
		}


	fprintf (f, "\n%s",output);

	fclose(f);
	return 0;
}
