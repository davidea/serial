/*
 * serial
 *
 * This program sends the AT commands to the modem and outputs
 * the responses.
 * if called with the flag -s or --signal it parse the response and extract
 * the signal from my huawei 909
 *
 * Copyright (C) 2018 davide ammirata
 * based on the idea of atinout from Håkon Løvdal
 * <hlovdal@users.sourceforge.net>
 * the original program , in my router doesn't work out of the box
 * then i try to rewrite it
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License V3 as published by
 *     the Free Software Foundation
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 */

#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <getopt.h>
#include "function.c"

/*  parameter
-h , help
-V , version
-v , verbose
-d , device
-c , command
-s , signal
-u , human
*/

static struct option long_options[] = {
	{"help", no_argument, NULL, 'h'},
	{"version", no_argument, NULL, 'V'},
	{"verbose", no_argument, NULL, 'v'},
  {"device", required_argument, NULL, 'd'},
	{"command", required_argument, NULL, 'c'},
	{"signal", required_argument, NULL, 's'},
	{"human", no_argument, NULL, 'u'},
	{NULL, 0, NULL, 0}
};
static const char *short_options = "hVvd:c:s:u";


int main (int argc, char *argv[]){
char response[1000];  												//buffer used to store the response frm the modem
char line[1000];
char output[1000];
char device[50];
int n,i,search_signal=0;
int option_index = 0;
int c;// cambiare nome variabile
int debug=0,human_readable=0;
char cmd[50]; /* accept command within 50 characters */
int dim; //dimensione del comando
const char* hostname = getenv("COLLECTD_HOSTNAME");

while (true) {

 c = getopt_long(argc, argv, short_options, long_options, &option_index);

 if (c == -1) {
   break;

 	}

 switch (c) {
 case 'h':
   help(argv[0]);
	 return EXIT_SUCCESS;

 case 'v':   //verbose
 	debug=1;
	break;

 case 'V':	//version
   printf("\n\nserial version " VERSION "\n");
   if (argc == 2) {
     printf("\nCopyright (C) 2018 davide ammirata <davide.ammirata@tiscali.it>\n"
            "This program comes with ABSOLUTELY NO WARRANTY.\n"
            "This is free software, and you are welcome to redistribute it under\n"
            "certain conditions; see http://www.gnu.org/licenses/gpl-3.0.html for details.\n");
     return EXIT_SUCCESS;
   }
   break;
 case 'd'://device
 		strncpy(device, optarg, (strlen (optarg)>49)?50:strlen (optarg)+1);
		printf("\n la dimensione di device è %d ed il contenuto è %s troncato a %d\n",strlen(device),device,(strlen (optarg)>49)?50:strlen (optarg)+1);
 		break;
 case 'c':	//command
 	  if(search_signal==0) {
			strncpy(cmd, optarg, (strlen (optarg)>49)?50:strlen (optarg)+1);
			printf("\n la dimensione di optarg è %d di cmd è %d ed il contenuto è %s troncato a %d\n",strlen(optarg),strlen(cmd),cmd,(strlen (optarg)>49)?50:strlen (optarg)+1);
		}
		add_cr(cmd); //add \r 0x0a to the command, it needs to submit the command to the modem
		dim = strlen(cmd);
		if(debug==1) {
			              printf("\nthe command you have entered after the funciont dd_cr is \n");
			              print_hex (cmd);
			              printf("\nthe lenght of the command is: %d byte\n",dim);
			              }
		break;
	case 's':		//signal
		if (strcmp ("me909",optarg)==0 ){
			strncpy(cmd, "at^HCSQ?\r", 9);
		} else if (strcmp ("qc25",optarg)==0 ) {
			printf("\nplaceholder for quectel qc25\n");
			return 0;
		} else {
			printf("\n\nyou must choose a modem type\navailable modem type are : me909 or qc25\n");
			return 0;
		}
		dim = strlen(cmd);
		search_signal=1;
		break;
	case 'u':		//print output in human readable format
		human_readable=1;
		break;
 case 0:
   if (strcmp("usage", long_options[option_index].name) == 0) {
     usage(argv[0]);
     return EXIT_SUCCESS;
   }
   break;

default:
   fprintf(stderr, "getopt returned character code 0x%02x\n", (unsigned int)c);
	 return EXIT_SUCCESS;
 }
}

if (argc == 1 ) {
	usage(argv[0]);
  return EXIT_FAILURE;
}

//here we open the modem the first time, becouse we must clear the rx buffer
//for this purpose we send the at command , it return ok, but we never use tis response

int fd = open (device, O_RDWR | O_NOCTTY | O_SYNC);
	if (fd < 0)
	{
        	printf ("error %d opening %s: %s", errno, device, strerror (errno));
	        return 0;
	}

set_interface_attribs (fd, B115200, 0);  // set speed to 115,200 bps, 8n1 (no parity)
set_blocking (fd, 0);                // set no blocking


write (fd, "at\r", 3);          // we have a  behavior where the buffer have some characters from the modem
sleep(1);                       // before we open the serial, than we need to read this characters to clean
n = read (fd, response, 1000);       // the buffer sending a simple command AT

if(debug==1) {
              printf("\nthe response from the first at command is \n' %s \n",response);
              print_hex(response);
              }
write (fd, cmd, dim);           // send the real command
sleep(1);
 n = read (fd, response, 1000);  // read up to 1000 characters if ready to read
 if(debug==1) {
	 						 printf("\nil comando passato è %s\n",cmd);
							 print_hex(cmd);
               printf("\nthe lenght of the response is %d\n",strlen(response));
               printf("\nthe response is \n\n%s",response);
              }



response_line(response,line);      //take only the line with the result
if(search_signal==0) {        // if we doens't use the command to report the signal we an output the buffer
  printf("\n%s\n",line);    //without other processing
} else {                    // if we need to report the signal we must
signal(line,output,human_readable,hostname);   // convert the output to something that have sense in the radio world
if(human_readable ==0) {
		printf("%s\n",output);
		write_log(output);
	}
}
return 0;
}
