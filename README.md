# serial

a simple program to send at command to the modem

parameter

-h , help

-V , version

-v , verbose

-d , device

-c , command

-s , signal   -s me909 , doesn't need to supply the -c command , implicit -c at^HCSQ?  -s qc25 future support for quectel QC25  
	the output is in the form PUTVAL .....


-u , human

-d /dev/ttyUSB2

-c atdt 0911234567

output with 

-s me909

in case we are on lte network

PUTVAL "OpenWrt/exec-lte/lte_rssi" N:-73.0
PUTVAL "OpenWrt/exec-lte/lte_rsrp" N:-99.0
PUTVAL "OpenWrt/exec-lte/lte_sinr" N:11.2
PUTVAL "OpenWrt/exec-lte/lte_rsrq" N:-5.5
PUTVAL "OpenWrt/exec-lte/lte_sum" N:-63.0:-88.0:12.2:-4.5

or in case we are in umts

PUTVAL "OpenWrt/exec-wcdma/wcdma_rssi" N:-75.0
PUTVAL "OpenWrt/exec-wcdma/wcdma_rscp" N:-77.0
PUTVAL "OpenWrt/exec-wcdma/wcdma_ecio" N:10.0



